from django.urls import path
from .views import PropertyRecommendation

app_name = 'properties'
urlpatterns = [
    path('api/get/info', PropertyRecommendation.as_view(), name='property-list')
]
