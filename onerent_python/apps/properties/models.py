from django.db import models

# Create your models here.
class Property(models.Model):

    class Meta:
        verbose_name = "Property"
        verbose_name_plural = "Properties"

    address = models.TextField()
    number_of_bathroom = models.FloatField(max_length=255)
    number_of_bedroom = models.PositiveIntegerField(default=0)
    longitude = models.FloatField(max_length=255)
    latitude = models.FloatField(max_length=255)
    price = models.FloatField(max_length=255)

    def __str__(self):
        return self.address

    def __unicode__(self):
        return self.address