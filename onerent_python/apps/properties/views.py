from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from .utils import get_distance, get_distance_geopy, recommended_price
from .models import Property
from .serializers import SimilarPropertiesSerializer


class PropertyRecommendation(RetrieveAPIView):

    def get(self, *args, **kwargs):
        try:
            obj = {
                'address': self.request.data.get('propertyAddress', None),
                'number_of_bedroom': self.request.data.get('numberOfBeds', None),
                'number_of_bathroom': self.request.data.get('numberOfBaths', None),
                'longitude': self.request.data.get('longitude', None),
                'latitude': self.request.data.get('latitude', None)
            }

            props = Property.objects.filter(
                number_of_bedroom=obj['number_of_bedroom'],
                number_of_bathroom__range=(obj['number_of_bathroom']-1, obj['number_of_bathroom']+1)
            )

            property_ids = []
            for prop in props:
                d = get_distance(obj['longitude'], obj['latitude'], prop.longitude, prop.latitude)

                # For more accurate calculation using a third-party library: GEOPY
                # d = get_distance_geopy(obj['longitude'], obj['latitude'], prop.longitude, prop.latitude)
                
                if d < 5.0:
                    property_ids.append(prop.id)
            queryset = props.filter(id__in=property_ids)
            price = recommended_price(obj, queryset)
            similar_properties = SimilarPropertiesSerializer(queryset, context={'property': obj}, many=True).data     

            response = {
                'address': obj['address'],
                'radius_coverage': 5,
                'recommended_price': price,
                'similar_properties': similar_properties
            }

            return Response(response, status=200)
        except Exception as e:
            response = {
                'error_msg': str(e)
            }
            return Response(response, status=500)