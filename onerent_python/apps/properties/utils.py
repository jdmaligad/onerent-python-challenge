from math import sin, cos, sqrt, atan2, radians
from geopy import distance
from django.db.models import Sum
from .models import Property

def get_distance(lon1, lat1, lon2, lat2):
    """
    NO THIRD-PARTY LIBRARY USED BUT LESS ACCURATE

    Harvesine formula:
        R = 6373.0 -> approximate radius of the earth
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = (sin(dlat/2))^2 + cos(lat1) * cos(lat2) * (sin(dlon/2))^2
        c = 2 * atan2( sqrt(a), sqrt(1-a) )
        d = R * c (where R is the radius of the Earth)

        d is in kilometers so we convert that to miles
        1km is equivalent to 0.6214 miles
    """
    R = 6373.0 
        
    x1 = radians(lon1)
    y1 = radians(lat1)
    x2 = radians(lon2)
    y2 = radians(lat2)

    x = x2 - x1
    y = y2 - y1

    a = sin(y / 2)**2 + cos(y1) * cos(y2) * sin(x / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    d = R * c

    return d * 0.6214


def get_distance_geopy(lon1, lat1, lon2, lat2):
    """
    WITH THE USE OF THIRD-PARTY LIBRARY + MORE ACCURATE
    """
    
    return distance.great_circle((lat1, lon1), (lat2, lon2)).miles


def recommended_price(property, similar_properties):
    price = 0
    """
    My approach in calculating the price is by separating the properties
    on how many are either more than the utilities of the given property
    or less than the given property. 
    
    But before that I check first whether the similar properties have 
    the same amount of utilities with the given property, once I filter that out, 
    I get the averaged sum of those filtered properties. 
    
    The same goes with filtering the properties for those that have more 
    or less utilities than the given property.
    
    If all conditions does not change the price of 0, then it would just
    calculate the average sum of all the similar properties regardless
    of any special condition.
    """
    has_same_utilities = similar_properties.filter(
        number_of_bedroom=property['number_of_bedroom'],
        number_of_bathroom=property['number_of_bathroom']
    )
    more_utilities = similar_properties.filter(
        number_of_bedroom__gte=property['number_of_bedroom'],
        number_of_bathroom__gte=property['number_of_bathroom']
    )
    less_utilities = similar_properties.filter(
        number_of_bedroom__lte=property['number_of_bedroom'],
        number_of_bathroom__lte=property['number_of_bathroom']
    )

    if has_same_utilities:
        price = has_same_utilities.aggregate(Sum('price')).get('price__sum', None) / has_same_utilities.count()
        return round(price, -1)
    
    if more_utilities and less_utilities:
        if more_utilities.count() > less_utilities.count():
            price = less_utilities.aggregate(Sum('price')).get('price__sum', None) / less_utilities.count()
        else:
            price = more_utilities.aggregate(Sum('price')).get('price__sum', None) / more_utilities.count()
        return round(price, -1)
    
    if price == 0:
        price = similar_properties.aggregate(Sum('price')).get('price__sum', None) / similar_properties.count()
        return round(price, -1)
