from rest_framework import serializers
from .models import Property
from .utils import get_distance, get_distance_geopy


class SimilarPropertiesSerializer(serializers.ModelSerializer):
    distance_from_parent = serializers.SerializerMethodField()

    class Meta:
        model = Property
        fields = ('id', 'address', 'number_of_bedroom', 'number_of_bathroom', 'price', 'distance_from_parent')

    def get_distance_from_parent(self, obj):
        parent = self.context.get('property', None)
        d = get_distance(
            parent['longitude'],
            parent['latitude'],
            obj.longitude,
            obj.latitude
        )

        # For more accurate calculation using a third-party library: GEOPY
        # d = get_distance_geopy(
        #     parent['longitude'],
        #     parent['latitude'],
        #     obj.longitude,
        #     obj.latitude
        # )

        return d