import json
from django.core.management.base import BaseCommand, CommandError
from apps.properties.models import Property

class Command(BaseCommand):
    help = 'Creates initial test data from test_data.json file'

    def handle(self, *args, **options):
        with open('../samples/test_data.json', encoding='utf-8') as test_data:
            test_data = json.loads(test_data.read())

            for obj_data in test_data:
                test_property = Property.objects.create(**obj_data)
                test_property.save()