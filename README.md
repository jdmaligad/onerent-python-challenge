# Python Backend Challenge
**From: Onerent**
**Solution by: Juan Alphonso D. Maligad**

# Walkthrough

## Setup
 1. Create `virtualenv / virtualenvwrapper` and activate
 2. Navigate to `/requirements/` folder and run the command: `pip install -r requirements.txt`
 3. Navigate to `/onrent_python/conf/` and create a file called `local_settings.py` and add this snippet of code:

    DATABASES =  {
    	'default':  {
    		ENGINE':  'django.db.backends.postgresql_psycopg2',
    		'NAME':  '**<Your_Database_Name>**',
    		'USER':  '**<postgres_user>**',
    		'PASSWORD':  '**<postgres_password>**',
    		'HOST':  'localhost',
    		'PORT':  '',
    	}
    }
   4. Navigate back to `/onerent_python/` and run migrations: `python manage.py migrate`
   5. From the same directory, run script to load/create test data into database: `python manage.py load_test_data`
   6. Finally, run the server: `python manage.py runserver`

## API Url

### GET
    /properties/api/get/info
Accepts form-data: 

    {
        "propertyAddress": string,
        "numberOfBaths": float,
        "numberOfBeds": int,
        "longitude": float,
        "latitude": float
    }

# Instructions

> Create a backend system that will be able to generate reports in JSON
> format. Setup a basic server using Python v3.6 or newer. Use Django or
> Flask whichever you prefer. Data should be retrieved from a sql
> database - preferrably postgres. Test data will be provided to you in
> JSON format and also sample inputs. The table schema should reflect
> the test data provided.

## Task

> Create an endpoint that will return a JSON summary report that
> retrieves the similar properties and a recommended price of the given
> property. The response should show the following information:
> 
>  - Property Address
>  - Radius Coverage in Miles
>  - Recommended Price
>  - Similar Properties:
> 	 - Property Address
> 	 - Price
> 	 - Distance from the Property Address in Miles

## Input

> The endpoint accepts the following values in json:
> 
>     property address: string
>     latitude: float
>     longitude: float
>     numberOfBeds: int
>     numberOfBaths: float

## Criteria

> Use the following criteria to determine similar properties:
> 
>  - Distance from the property should be within 5 miles (< 5 miles)
>  - Should have the same number of beds from the input
>  - The number of bathrooms should be +- 1 from the input

## Formulation

### Price recommendation

> Given the similar properties you have retrieved, what statistics
> formula would you use to determine the price for the property? (e.g.
> average, median, etc.) Add code comment explaining as to why you used
> that formula.
> 
> Use of python libraries are acceptable. Good luck!
